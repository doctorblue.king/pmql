<?php
  session_start();
?>
<!DOCTYPE HTML>
<html lang="en" >
<html>
<head>
  <title>Login</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href="./../styles/login_style.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link href='https://fonts.googleapis.com/css?family=Titillium+Web:400,300,600' rel='stylesheet' type='text/css'>  
  <link href='https://fonts.googleapis.com/css?family=Titillium+Web:400,300,600' rel='stylesheet' type='text/css'> 
  <script src="https://unpkg.com/@lottiefiles/lottie-player@latest/dist/lottie-player.js"></script>
</head>

<body class="body">
	
<div class="login-page">
  <div class="form">
  <?php
        require('./database/controls.php');
        $controls =new controls();

        $message="";
        $user_name ="";
        $password = "";
        if(isset($_POST['btn_login'])){
          $user_name = $_POST['txt_user_name'];
          $password = $_POST['txt_password'];

          if(empty($user_name)){

            $message ="Tên tài khoản không được để trống !";

          }elseif(empty($password)){
            
            $message ="Mật khẩu không được để trống !";

          }else{
            if(strlen($password) < 8){

              $message = "Mật khẩu quá ngắn";
              
            }else{
              $hasUser = $controls->checkUser($user_name);
              $is_pass = $controls->checkPassword($password,$user_name);
              
              if($hasUser == 0){
                $message = "Tên tài khoản không chính xác!";
              }else{
                  if($is_pass == 0){
                    $message = "Mật khẩu không chính xác !";
                  }else{
                      $result = $controls->getUser($user_name);

                      $row = mysqli_fetch_row($result);
                      $_SESSION['Account'] =$row;

                      $message = $row[8];
                      
                      if($row[8]==1){
                          header("Location: ./admin/books.php");
                      }else{
                          $_SESSION['SACH_MM'] = array();
                          header("Location: ./user/books.php");
                      }
  
                  }
              }
            }
          }
        }
        if(isset($_POST['btn_sign_up'])){
          header("Location: ./signup.php");
        }
      ?>

    <form method="POST">
      <lottie-player src="https://assets4.lottiefiles.com/datafiles/XRVoUu3IX4sGWtiC3MPpFnJvZNq7lVWDCa8LSqgS/profile.json"  background="transparent"  speed="1"  style="justify-content: center;" loop  autoplay></lottie-player>
      <input type="text" placeholder="&#xf007;  username" name="txt_user_name" value = '<?php echo $user_name; ?>'/>
      <input type="password" placeholder="&#xf023;  password" name="txt_password" value = '<?php echo $password; ?>'/>
      <button name="btn_login">LOGIN</button>
      <span class="message" style="color: indianred; font-weight: bold;"><?php echo $message; ?></span>
      <button name="btn_sign_up">SIGN UP</button>

    </form>
  </div>
</div>

</body>
</html>

