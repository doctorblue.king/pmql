<?php
    include('admin.php');
    include('user.php');
    class Config{

        public function convertAccount($row){
            $user_name = $row[1];
            $full_name = $row[2];
            $email = $row[3];
            $password = $row[4];
            $date_of_birth = $row[5];
            $money = $row[6];
            $imageUrl =  $row[7];
            $is_admin = $row[8];

            if($is_admin){
                // Trả về kiểu admin ở đây
                return new Admin($user_name, $full_name, $email, $password, $date_of_birth, $money,$imageUrl, $is_admin);
            }else{
                //Trả về kiểu user
                return new User($user_name, $full_name, $email, $password, $date_of_birth, $money,$imageUrl, $is_admin);
            }
        }
    }

?>