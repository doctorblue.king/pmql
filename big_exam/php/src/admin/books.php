<?php
  session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Books (Admin)</title>
    <link rel="stylesheet" href="./../../styles/books_admin_styles.css">
    <link rel="stylesheet" href="./../../styles/nav_styles.css">

</head>
<body class="body">
<?php
        require('./../database/controls.php');
        require('./../models/user_config.php');
        include('./../models/book.php');
        $account = $_SESSION['Account'];
        $config = new Config();
        $control = new controls();
        $admin = $config->convertAccount($account);

        if($account == null || $admin->is_admin == false){
            header("Location: ./../logout.php");
        }

        $title = "Hi, ". $admin->user_name . " (Admin)";
        $arr = $control->getAllBook($admin,$account['id']);
        $message = "";

        foreach($arr as $value){
            if(isset($_POST['btn_update'.$value['id']])){
                $_SESSION['UPDATE_BOOK'] = $value;
                header('Location: ./update_books.php');
            }
            if(isset($_POST['btn_delete'.$value['id']])){
                $num = $control->checkBookRentById($value['id'],$admin);
                if($num == 0){
                    $run = $admin->deleteBook($value['id']);
                    unlink('./../../assets/uploads/books/'.$value['imageUrl']);
                    if($run){
                        echo "<script> alert('Xóa thành công !'); </script>";
                    }else{
                        echo "<script> alert('Xóa thất bại !'); </script>";
                    }
                    header("Refresh:0");
                }else{
                    $message = 'Không thể xóa cuốn sách '.$value['title'].' vì có user đang mượn';
                }
            }
        }

    ?>
    <div style="float: left;">
        <h2><?php echo $title; ?></h2>
        <div class="navbar">
            <div class="dropdown">
              <img src="./../../assets/images/user.png" width="32px" height="32px" class="dropbtn">
                <i class="fa fa-caret-down"></i>
            </img>
              <div class="dropdown-content">
                <a href="./add_books.php">Nhập sách</a>
                <a href="./../profile.php">Chỉnh sửa thông tin của bạn</a>
                <a href="./top_up.php">Nạp tiền cho người dùng</a>
                <a href="./../logout.php">Logout</a>
              </div>
            </div>
          </div> 
    </div>
    <div style="float: left; text-align: center; width: 1920px;">
    <span style="font-size: 30px; color: white"><?php echo $message; ?></span>
        <div class="books-page">
        <?php 
                foreach ($arr as $key => $value) {
                    echo '<div class="book">
                    <img src="./../../assets/uploads/books/'.$value['imageUrl'].'"width="340px" style="max-height: 200px;"/>
                    <div style="width: 340px;height: 220px;">
                        <p class="title">'.$value['title'].'</p>
                        <p class="description">'.$value['description'].'</p>
                        <p class="author">Tác giả : '.$value['author'].'</p>
                        <p class="amount">Còn lại : '.$value['amount'].'</p>
                        <p class="amount">Giá thuê: '.$value['rent_cost'].'</p>
                    </div>
                    <form method="POST" class="form">
                        <button name="btn_update'.$value['id'].'">Sửa</button>
                        <button name="btn_delete'.$value['id'].'" style="background-color: indianred;">Xóa</button>
                    </form>
                </div>';
                } 
                
            ?>
        </div>
    </div>
</body>
</html>