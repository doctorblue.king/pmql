<?php
  session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Profile</title>
    <link rel="stylesheet" type="text/css" href="./../../styles/top_up_styles.css">
</head>

<body class="body">
    <div class="profile-page">
    <?php
        require('./../database/controls.php');
        require('./../models/user_config.php');
        include('./../models/book.php');
        $account = $_SESSION['Account'];
        $config = new Config();
        $control = new controls();
        $admin = $config->convertAccount($account);

        if($account == null || $admin->is_admin == false){
            header("Location: ./../logout.php");
        }

        $user_name = "";
        $money = "";
        $message = "";
        if(isset($_POST['btn_save'])){
            $user_name = $_POST['txt_user_name'];
            $money = $_POST['txt_money'];
            if(empty($user_name)){
                $message ="Bạn chưa nhập tên tài khoản !";

            }elseif(empty($money)){
                
                $message ="Bạn muộn nạp bao nhiêu vậy ?";
    
            }elseif($money < 10){
                $message ="Số tiền phải lớn hơn 10$";
            }else{
                $run = $admin->topUp($user_name, $money);
                if($run != 0){
                    $message = "Nạp tiền cho $user_name thành công !";
                    $user_name ="";
                    $money ="";
                }else{
                    $message = "Nạp tiền cho $user_name thất bại !";
                }
            }
        }
        if(isset($_POST['btn_back'])){
            header("Location: ./books.php");
        }


    ?>
        <div class="right">
            <form class="form1" method="POST">
                <p>User Name</p>
                <input type="text" placeholder="Nhập user name" name="txt_user_name" value="<?php echo $user_name ?>"/>
                <p>Số tiền muốn nạp</p>
                <input type="number" step="0.01" placeholder="Nhập số tiền muốn nạp ( $ )" name="txt_money" value="<?php echo $money ?>"/>
                <span class="message" style="color: indianred; font-weight: bold;"><?php echo $message; ?></span>
                <div class="bottom">
                    <div>
                        <button name="btn_back">BACK</button>
                    </div>
                    <div style="margin-left: 200px;">
                        <button name="btn_save">SAVE</button>
                    </div>
                </div>
            </form>
        </div>
  
        
    </div>
</body>

</html>