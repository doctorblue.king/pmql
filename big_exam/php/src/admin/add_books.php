<?php
  session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Thêm sách</title>
    <link rel="stylesheet" type="text/css" href="./../../styles/add_book.css">
</head>

<body class="body">
    <div class="profile-page">
        <?php
        require('./../database/controls.php');
        require('./../models/user_config.php');
        include('./../models/book.php');
        
        $account = $_SESSION['Account'];
        $config = new Config();
        $admin = $config->convertAccount($account);
        $book_name = "";
        $author = "";
        $description = "";
        $rent_cost = "";
        $amount = "";
        $img_url ="";
        $message ="";

        if($account == null || $admin->is_admin == false){
            header("Location: ./../logout.php");
        }


        if(isset($_POST['btn_save'])){
            $book_name = $_POST['book_name'];
            $author = $_POST['author'];
            $description = $_POST['description'];
            $rent_cost = $_POST['rent_cost'];
            $amount = $_POST['amount'];
            //$img_url = $_POST['book_name'];
            if(empty($book_name)){
                $message = "Bạn định để trống tên sách hả ?";
            }elseif(empty($author)){
                $message = "Sách không có tác giả hay sao ?";
            }elseif(empty($description)){
                $message = "Mô tả sách trống kìa !";
            }elseif(empty($rent_cost)){
                $message = "Bạn không muốn nhập giá thuê hả ?";
            }elseif(empty($amount)){
                $message = "Số lượng sách trống kìa !";
            }elseif($amount <= 0){
                $message = "Số lượng sách nhỏ vậy à ?";
            }else{
                $target_dir = "./../../assets/uploads/books/";
                $target_file = $target_dir . $book_name . basename($_FILES["fileToUpload"]["name"]);
                $uploadOk = 1;
                $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

                if(empty($_FILES["fileToUpload"]["tmp_name"])){
                    $message = "Bạn chưa chọn ảnh !";
                }else{
                $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
                if($check !== false) {
                    $uploadOk = 1;
                } else {
                    $message = "File is not an image.";
                    $uploadOk = 0;
                }

                if ($uploadOk == 0) {
                    $message = "Sorry, your file was not uploaded.";
                } else {
                    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
                        $img_url = $book_name . htmlspecialchars( basename( $_FILES["fileToUpload"]["name"]));
                        $book = new Book($book_name, $author, $description, $rent_cost,$amount,$img_url);
                        $book->addBook($admin);
                } else {
                    $message = "Sorry, there was an error uploading your file.";
                    }
                }
                
            }
        }
        }
        if(isset($_POST['btn_back'])){
            header('Location: ./books.php');
        }
        ?>
        <div class="left">
            <img src="<?php echo "./../../assets/uploads/books/" . $img_url ?>"  width="340px" style="max-height: 200px;" class="img" name="img_book" />
        
        </div>
        <div class="right">
            <form class="form1" method="POST" enctype="multipart/form-data">
                <p>Book name</p>
                <input type="text" placeholder="Tên Sách" name="book_name" value="<?php echo $book_name;?>"/>
                <p>Author</p>
                <input type="text" placeholder="Tác giả" name="author" value="<?php echo $author;?>"/>
                <p>Description</p>
                <input type="text" placeholder="Mô tả"  name="description" value="<?php echo $description ;?>"/>
                <p>Rent cost</p>
                <input type="number" step="0.01" placeholder="Gía thuê" name="rent_cost" value="<?php echo $rent_cost;?>"/>
                <p>Amount</p>
                <input type="number" placeholder="Số lượng" name="amount"value="<?php echo $amount;?>"/>
                <p>Choose Image</p>
                <input type="file" name="fileToUpload" id="fileToUpload" >
                <span class="message" style="color: indianred; font-weight: bold; "><?php echo $message; ?></span>
                <div class="bottom">
                        <div style="margin-top: 24px;" >
                            <button name="btn_back" >BACK</button>
                        </div>
                        <div style="margin-top: 24px; margin-left: 200px;">
                            <button name="btn_save">SAVE</button>
                        </div>
                </div>
            </form>
        </div>

    </div>
</body>

</html>