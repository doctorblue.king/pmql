<?php
  session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> Your books</title>
    <link rel="stylesheet" href="./../../styles/your_books_styles.css">
    <link rel="stylesheet" href="./../../styles/nav_styles.css">
</head>

<body class="body">
    <?php
        require('./../database/controls.php');
        require('./../models/user_config.php');
        include('./../models/book.php');
        
        $config = new Config();
        $control = new controls();
        $account = $_SESSION['Account'];

        $result = $control->getUser($account[1]);

        $row = mysqli_fetch_row($result);
        $_SESSION['Account'] = $row;
        $account = $_SESSION['Account'];


        $user = $config->convertAccount($account);

        
        if($account == null || $user->is_admin){
            header("Location: ./../logout.php");
        }
        $title = "Hi, ". $user->user_name . " (User)";
        $arr = $control->getUserRentBook($account[0]);
        $user_money = $user->money;
        $count_da_muon = mysqli_num_rows($arr);
        $today = date("Y/m/d");
        $message = $_SESSION['MESS'];

        foreach ($arr as $key => $value) {

            if(isset($_POST['btn_tra'.$value['id']])){
                $time_today = strtotime($today);
                $time_due = strtotime($value['due_date']);
                $datediff = $time_today - $time_due;
                $day = floor($datediff / (60*60*24));
                if($day <= 0){
                    $control->minusBookAmount($value['amount']+1, $value['book_id']);
                    $control->deleteRentBook($value['book_id'], $value['user_id']);
                    $_SESSION['MESS']="";
                    header("Refresh:0");

                }else{
                    $fine = $value['rent_cost']/100.00 *5*$day;
                    $_SESSION['MESS'] = "Bạn trả muộn $day ngày nên bị phạt 5% số tiền thuê cuốn sách * số ngày trả muộn = $fine $ !";
                    $control->minusBookAmount($value['amount']+1, $value['book_id']);
                    $control->deleteRentBook($value['book_id'], $value['user_id']);
                    $control->deduction($user->money - $fine,$user->user_name);
                    header("Refresh:0");
                } 


            }
        } 

    ?>
    <div style="float: left;">
        <h2><?php echo $title; ?></h2>
        <div class="navbar">
            <div class="dropdown">
                <img src="./../../assets/images/user.png" width="32px" height="32px" class="dropbtn">
                <i class="fa fa-caret-down"></i>
                </img>
                <div class="dropdown-content">
                <a href="./../profile.php">Chỉnh sửa thông tin</a>
                <a href="./books.php">Home page</a>
                <a href="./../logout.php">Logout</a>
                </div>
            </div>
        </div>
    </div>
    <div style="float: left; text-align: center; width: 1920px; position: relative;">
    <span style="font-size: 30px; color: white"><?php echo $message; ?></span>

        <div class="books-page">
        <?php 
                foreach ($arr as $key => $value) {
                    echo '<div class="book">
                    <img src="./../../assets/uploads/books/'.$value['imageUrl'].'"width="340px" style="max-height: 200px;"/>
                    <div style="width: 340px;height: 220px;">
                        <p class="title">'.$value['title'].'</p>
                        <p class="description">'.$value['description'].'</p>
                        <p class="author">Tác giả : '.$value['author'].'</p>
                        <p class="amount">Giá thuê: '.$value['rent_cost'].'</p>
                        <p class="amount">Ngày phải mượn: '.$value['borrowing_date'].'</p>
                        <p class="amount">Ngày phải trả: '.$value['due_date'].'</p>
                    </div>
                    <form method="POST" class="form">
                        <button name="btn_tra'.$value['id'].'">Trả sách</button>
                    </form>
                </div>';
                } 
                
            ?>
         
            <div class="borrow">
                <p class="user-money"">Bạn hiện có : <?php echo $user_money; ?></p>
            <p class="des">Số sách bạn đang mượn: <?php echo $count_da_muon; ?></p>
            </div>
        </div>

</body>

</html>