<?php
  session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Books (User)</title>
    <link rel="stylesheet" href="./../../styles/books_user_styles.css">
    <link rel="stylesheet" href="./../../styles/nav_styles.css">

</head>
<body class="body">
    <?php
        require('./../database/controls.php');
        require('./../models/user_config.php');
        include('./../models/book.php');
        
        $config = new Config();
        $control = new controls();
        $account = $_SESSION['Account'];

        $result = $control->getUser($account[1]);

        $row = mysqli_fetch_row($result);
        $_SESSION['Account'] =$row;
        $account = $_SESSION['Account'];
        $_SESSION['MESS']=null;


        $user = $config->convertAccount($account);

        
        if($account == null || $user->is_admin){
            header("Location: ./../logout.php");
        }

        $title = "Hi, ". $user->user_name . " (User)";
        $arr = $control->getAllBook($user,$account[0]);
        $arr_thue = $_SESSION['SACH_MM'];
        $count_mm = count($arr_thue);

        $message = "";
        $user_money = $user->money;
        $rent_money = $_SESSION['RENT_MONEY'];
        $today = date("Y/m/d");
        $date = new DateTime($today);
        $due_date = $_SESSION['DUE_DATE'];
        $due = 2;

        $arr_sach_da_muon = $control->getRentBook($account[0]);
        $count_da_muon = mysqli_num_rows($arr_sach_da_muon);

        foreach ($arr as $key => $value) {

            if(isset($_POST['btn_thue'.$value['id']])){
                if($count_mm + $count_da_muon >= 3){
                    $message = "Tối đa bạn chỉ có thể mượn 3 cuốn sách";
                }else{
                    $message = "";
                    $_SESSION['SACH_MM'][$key] = $value;
                    $arr_thue = $_SESSION['SACH_MM'];
                    $count_mm = count($arr_thue);
                    $due = $count_mm +$due;
                    $date->modify('+'.$due.' day');
                    $due_date = $date->format('Y/m/d');
                    $rent_money =0;
                    $_SESSION['DUE_DATE'] = $due_date;
                    foreach($arr_thue as $val){
                        $rent_money = $rent_money+$val['rent_cost'];
                        $_SESSION['RENT_MONEY'] = $rent_money;
                    }
                }
            }
        } 
        if(isset($_POST['btn_clear_list'])){
            clear();
        }
        if(isset($_POST['btn_muon'])){
            $rent_money = $_SESSION['RENT_MONEY'];
            if($user_money*2 < $rent_money ){
                $message = "Số tiền trong tài khoản của bạn phải gấp đôi số tiền phải trả mới có thể thuê";
            }else{
                if($count_mm>0){
                    foreach($arr_thue as $value){
                        $control->rentBook($value['id'],$account[0],$today,$_SESSION['DUE_DATE']);
                        $control->minusBookAmount($value['amount']-1,$value['id']);
                    }
                    $control->deduction($user_money-$rent_money, $user->user_name);
                    clear();
                }
            }
        }

        function clear(){
            $_SESSION['SACH_MM']=array();
            $arr_thue = array();
            $count_mm = 0;
            $_SESSION['RENT_MONEY'] = 0;
            $rent_money =0;
            $message="";
            $_SESSION['DUE_DATE'] = "";
            $due_date = "";
            $due = 2;
            header("Refresh:0");
        }

    ?>
    
    <div style="float: left;">
        <h2><?php echo $title; ?></h2>
        <div class="navbar">
            <div class="dropdown">
              <img src="./../../assets/images/user.png" width="32px" height="32px" class="dropbtn">
                <i class="fa fa-caret-down"></i>
            </img>
              <div class="dropdown-content">
                <a href="./../profile.php">Chỉnh sửa thông tin</a>
                <a href="./your_books.php">Sách bạn đã thuê</a>
                <a href="./../logout.php">Logout</a>
              </div>
            </div>
          </div> 
    </div>
    <div style="float: left; text-align: center; width: 1920px; position: relative;">
    <span style="font-size: 30px; color: white"><?php echo $message; ?></span>
        <div class="books-page">
            <?php 
                foreach ($arr as $key => $value) {
                    // $arr[3] will be updated with each value from $arr...
                    echo '<div class="book">
                    <img src="./../../assets/uploads/books/'.$value['imageUrl'].'"width="340px" style="max-height: 200px;"/>
                    <div style="width: 340px;height: 220px;">
                        <p class="title">'.$value['title'].'</p>
                        <p class="description">'.$value['description'].'</p>
                        <p class="author">Tác giả : '.$value['author'].'</p>
                        <p class="amount">Còn lại : '.$value['amount'].'</p>
                        <p class="amount">Giá thuê: '.$value['rent_cost'].'</p>
                    </div>
                    <form method="POST" class="form">
                        <button name="btn_thue'.$value['id'].'">Thuê</button>
                    </form>
                </div>';
                } 
                
            ?>

        </div>

        <div class="borrow">
            <p class="user-money"">Bạn hiện có : <?php echo $user_money; ?></p>
            <p class="des">Số sách bạn đang mượn: <?php echo $count_da_muon; ?></p>
            <p class="des">Bạn muốn mượn thêm : <?php echo $count_mm;?></p>
            <p class="des">Bạn cần trả : <?php echo $rent_money; ?></p>
            <p class="des">Ngày phải trả : <?php echo $due_date; ?></p>
            <form method="POST" class="form2">
                <button name="btn_muon">Xác nhận Thuê sách</button>
                <button name="btn_clear_list" style="background: indianred;">Clear List</button>
            </form>
        </div>
    </div>

</body>
</html>