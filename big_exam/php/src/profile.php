<?php
  session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Profile</title>
    <link rel="stylesheet" type="text/css" href="./../styles/profile_style.css">
</head>

<body class="body">
<?php
        require('./database/controls.php');
        require('./models/user_config.php');
        include('./models/book.php');
        $account = $_SESSION['Account'];
        $config = new Config();
        $control = new controls();
        $acc = $config->convertAccount($account);

        if($account == null){
            header("Location: ./../login.php");
        }
        $message="";
        $user_name = $acc->user_name;
        $password =  $acc->password;
        $re_password = $acc->password;
        $full_name = $acc->full_name;
        $email = $acc->email;
        $date_of_birth = $acc->date_of_birth;
        $img_url = "avatar.png";

        if(!empty($acc->imageUrl)){
          $img_url = $acc->imageUrl;
        }
        
        if(isset($_POST['btn_save'])){ 
            $password = $_POST['txt_password'];
            $re_password = $_POST['txt_re_password'];
            $full_name = $_POST['txt_full_name'];
            $email = $_POST['txt_email'] ;
            $date_of_birth = $_POST['txt_date_of_birth'];  

           if(empty($full_name)){
              $message ="Họ tên không được để trống !";
  
            }elseif(empty($email)){
              $message ="Email không được để trống !";
  
            }elseif(empty($password)){
              $message ="Mật khẩu không được để trống !";
  
            }elseif(empty($date_of_birth)){
              $message ="Ngày sinh của bạn là gì ?";
            }
            else{
              if(strlen($password) < 8){
                $message = "Mật khẩu quá ngắn";
                
              }elseif($password != $re_password){
                $message = "Mật khẩu bạn nhập không khớp";
              }
              else{
                $acc->full_name = $full_name;
                $acc->email = $email;
                $acc->password = $password;
                $acc->date_of_birth = $date_of_birth;
                $is_success = 0;

                $target_dir = "./../assets/uploads/avatars/";
                $target_file = $target_dir .$user_name . basename($_FILES["fileToUpload"]["name"]);
                $uploadOk = 1;
                $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

                if(empty($_FILES["fileToUpload"]["tmp_name"])){
                  $is_success = $control->updateProfile($acc);
                }else{
                $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
                if($check !== false) {
                    $uploadOk = 1;
                } else {
                    $message = "File is not an image.";
                    $uploadOk = 0;
                }

                if ($uploadOk == 0) {
                    $message = "Sorry, your file was not uploaded.";
                } else {
                    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
                      
                      $old_avt_url = $target_dir.$acc->imageUrl;
                      $img_url = $user_name . htmlspecialchars(basename( $_FILES["fileToUpload"]["name"]));
                      $acc->imageUrl = $img_url; 
                      $is_success = $control->updateProfile($acc);
                      if(!empty($old_avt_url)){
                        unlink($old_avt_url);
                      }
                    } else {
                    $message = "Sorry, there was an error uploading your file.";
                    }
                }                
            } 
            if($is_success != 0){
              $message = "Update thành công !";
    
              $account[2] = $acc->full_name;
              $account[3] = $acc->email;
              $account[4] = $acc->password;
              $account[5] = $acc->date_of_birth;
              $account[7] = $acc->imageUrl;
              $_SESSION['Account'] = $account;
              if($acc->is_admin){
                header("Location:./admin/books.php");

              }else{
                header("Location:./user/books.php");

              }

            }else{
              $message = "Update thất bại !";
            }

              }
            } 
        }
        if(isset($_POST['btn_delete'])){
           $is_success = $control->deleteUser($acc);
          if($is_success == 1){
            header("Location:./logout.php");
          }else{
            $message = $is_success;
          }

        }
        

?>
    <div class="profile-page">
        <div class="left">
            <img src="<?php echo "./../assets/uploads/avatars/".$img_url; ?>" / width="192px" height="192px" class="img" />
        </div>
        <div class="right">
            <form class="form1" method="POST"  enctype="multipart/form-data">
                <lottie-player
                    src="https://assets4.lottiefiles.com/datafiles/XRVoUu3IX4sGWtiC3MPpFnJvZNq7lVWDCa8LSqgS/profile.json"
                    background="transparent" speed="1" style="justify-content: center;" loop autoplay></lottie-player>
                <p>Full name</p>
                <input type="text" placeholder="full name" name="txt_full_name" value='<?php echo $full_name ?>'/>
                <p>Email</p>
                <input type="email" placeholder="email address" name="txt_email" value='<?php echo $email ?>'/>
                <p>User Name</p>
                <input type="text" placeholder="pick a username" name="txt_user_name" disabled value='<?php echo $user_name ?>'/>
                <p>Date of birth</p>               
                <input type="date" name="txt_date_of_birth" value="<?php echo $date_of_birth?>"/>
                <p>Password</p>
                <input type="password" placeholder="set a password" name="txt_password" value='<?php echo $password ?>'/>
                <p>Confirm your password</p>
                <input type="password" placeholder="confirm password" name="txt_re_password" value='<?php echo $re_password ?>'/>
                <p>Choose Image</p>
                <input type="file" name="fileToUpload" id="fileToUpload" >

                <span class="message" style="color: indianred; font-weight: bold; margin-top: 15px;"><?php echo $message; ?></span>
                <div style="margin-top: 15px;">
                    <button name="btn_delete" style="background-color: indianred;">DELTETE</button>
                </div>
                <div style="margin-left: 248px;">
                <button name="btn_save">SAVE</button>
                </div>

            </form>
        </div>
        <div class="bottom">
            <form class="form2" method="POST">

            </form>
        </div>
    </div>
</body>

</html>