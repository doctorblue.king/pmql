-- CREATE DATABASE LIBRARY;

CREATE TABLE user (
    id INT AUTO_INCREMENT PRIMARY KEY,
    username VARCHAR(30) NOT NULL,
    fullname VARCHAR(60) NOT NULL,
    email VARCHAR(50),
    password CHAR(12),
    date_of_birth DATE,
    money DECIMAL(12,2),
    imageUrl VARCHAR(200),
    isAdmin BOOLEAN DEFAULT false
);

CREATE TABLE book(
    id INT AUTO_INCREMENT PRIMARY KEY,
    title VARCHAR(60) NOT NULL,
    author VARCHAR(60) NOT NULL,
    description VARCHAR(255),
    rent_cost DECIMAL(6,2) NOT NULL,
    amount INT,
    imageUrl VARCHAR(200)
);

CREATE TABLE rented_books(
    id INT AUTO_INCREMENT PRIMARY KEY,
    book_id INT NOT NULL,
    user_id INT NOT NULL,
    borrowing_date DATE NOT NULL,
    due_date DATE NOT NULL,
    FOREIGN KEY (book_id) REFERENCES book(id),
    FOREIGN KEY (user_id) REFERENCES user(id)
);

INSERT INTO user(username,fullname,email,password,date_of_birth,money,imageUrl,isAdmin) VALUES("vantan","Nguyen Van Tan","Doctorblue.king@gmai.com","12345678","2000-07-26",0,"",true);
INSERT INTO user(username,fullname,email,password,date_of_birth,money,imageUrl,isAdmin) VALUES("docdoc","Tran Thi B","tranB123g@gmai.com","12345678","1999-10-08",0,"",true);
INSERT INTO user(username,fullname,email,password,date_of_birth,money,imageUrl,isAdmin) VALUES("cecece","Nguyen Thu C","Thucc456@gmai.com","12345678","1990-01-12",0,"",true);