<?php
    include('connect.php');

    class Controls{

        public function getUser($username) {
            global $conn;
            $run = mysqli_query($conn,"SELECT * from user where username = '$username'");
            return $run;
        }

        public function checkUser($username) {
            global $conn;
            $sql = "SELECT * from user where username = '$username'";
            $run = mysqli_query($conn, $sql);
            $num = mysqli_num_rows($run);
            return $num;
        }
        public function checkPassword($password, $username){
            global $conn;
            $sql = "SELECT * FROM user WHERE username = '$username' AND password ='$password' ";
            $run = mysqli_query($conn, $sql);
            $num = mysqli_num_rows($run);
            return $num;
        }
        public function getAllBook($account,$user_id){
            global $conn;
            $sql = "";
            if($account->is_admin){
                $sql = "SELECT * FROM book";
            }else{
               $sql = "SELECT * FROM `book` WHERE amount > 0 AND id NOT IN(SELECT book_id FROM rented_books WHERE user_id = '$user_id')";
            }
            $run = mysqli_query($conn, $sql);
            return $run;
        }
        public function updateProfile($account){
            global $conn;
            $sql = "UPDATE user SET fullname='$account->full_name', email='$account->email',password='$account->password',date_of_birth='$account->date_of_birth',imageUrl='$account->imageUrl' WHERE username='$account->user_name'";
            $run = mysqli_query($conn, $sql);
            return $run; 
        }
        public function deleteUser($account){
            global $conn;
            if($account->is_admin){
                return "Bạn không thể xóa tài khoản của mình vì bạn là admin";
            }else{
                $sql = "DELETE FROM user WHERE username='$account->user_name'";
                $run = mysqli_query($conn, $sql);
                return $run; 
            }
        }
        function getRentBook($user_id){
            global $conn;
            if(!$user->is_admin){
                $sql = "SELECT * FROM rented_books WHERE user_id='$user_id'";
                $run = mysqli_query($conn, $sql);
                return $run;
            }
        }
        function rentBook($book_id,$user_id,$borrowing_date, $due_date){
            global $conn;
            if(!$user->is_admin){
                $sql = "INSERT INTO rented_books(book_id,user_id,borrowing_date,due_date) VALUES ('$book_id','$user_id','$borrowing_date','$due_date')";
                $run = mysqli_query($conn, $sql);
                return $ru ;
            }
        }
        function deduction($money, $user_name){
            global $conn;
            $sql = "UPDATE user SET money='$money' WHERE username='$user_name'";
            $run = mysqli_query($conn, $sql);
            return $run; 
        }

        function minusBookAmount($amount, $id){
            global $conn;
            $sql = "UPDATE book SET amount='$amount' WHERE id='$id'";
            $run = mysqli_query($conn, $sql);
            return $run; 
        }

        function getUserRentBook($user_id){
            global $conn;
            if(!$user->is_admin){
                $sql = "SELECT * FROM rented_books INNER JOIN book ON book.id = rented_books.book_id WHERE user_id='$user_id' ORDER BY rented_books.due_date ASC";
                $run = mysqli_query($conn, $sql);
                return $run;
            }
        }
        function deleteRentBook($book_id, $user_id){
            global $conn;
            if(!$user->is_admin){
                $sql = "DELETE FROM rented_books WHERE user_id='$user_id' AND book_id='$book_id' ";
                $run = mysqli_query($conn, $sql);
                return $run;
            }
        }
        function checkBookRentById($book_id,$admin){
            global $conn;
            if($admin->is_admin){
                $sql = "SELECT * FROM rented_books WHERE book_id='$book_id' ";
                $run = mysqli_query($conn, $sql);
                $num = mysqli_num_rows($run);
                return $num;
            }
        }
    }
?>