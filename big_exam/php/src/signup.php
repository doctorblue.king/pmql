<!DOCTYPE HTML>
<html lang="en" >
<html>
<head>
  <title>Sign Up</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href="./../styles/signup_style.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link href='https://fonts.googleapis.com/css?family=Titillium+Web:400,300,600' rel='stylesheet' type='text/css'>  
  <script src="https://unpkg.com/@lottiefiles/lottie-player@latest/dist/lottie-player.js"></script>
</head>

<body class="body">
  
<div class="login-page">
  <div class="form">
  <?php
        require('./database/controls.php');
        require('./models/user.php');
        $controls = new controls();


        $message="";
        $user_name ="";
        $password = "";
        $re_password ="";
        $full_name = "";
        $email = "";
        $date_of_birth = "";
        if(isset($_POST['btn_sign_up'])){
          $user_name = $_POST['txt_user_name'];
          $password = $_POST['txt_password'];
          $re_password = $_POST['txt_re_password'];
          $full_name = $_POST['txt_full_name'];
          $email = $_POST['txt_email'] ;
          $date_of_birth  = $_POST['txt_date_of_birth']; 
          
          if(empty($full_name)){
            $message ="Họ tên không được để trống !";

          }elseif(empty($email)){
            $message ="Email không được để trống !";

          }
          elseif(empty($user_name)){
            $message ="Tên tài khoản không được để trống !";

          }elseif(empty($password)){
            $message ="Mật khẩu không được để trống !";

          }elseif(empty($date_of_birth)){
            $message ="Ngày sinh của bạn là gì ?";
          }
          else{
            if(strlen($password) < 8){
              $message = "Mật khẩu quá ngắn";
              
            }elseif($password != $re_password){
              $message = "Mật khẩu bạn nhập không khớp";
            }
            else{
              $hasUser = $controls->checkUser($user_name);
              if($hasUser !=0){
                $message = "Tên tài khoản đã tồn tại";
              }else{
                $user = new User($user_name,$full_name,$email,$password,$date_of_birth,0.00,'',0);
                $run = $user->signUp();
                if($run){
                  $message = "Đăng kí thành công";
                  header("Location: ./login.php");
                }
                else{
                  $message = "Đăng kí thất bại";
                }
              }
            }
          }
        }
      ?>




    <form method="POST">
      <lottie-player src="https://assets4.lottiefiles.com/datafiles/XRVoUu3IX4sGWtiC3MPpFnJvZNq7lVWDCa8LSqgS/profile.json"  background="transparent"  speed="1"  style="justify-content: center;" loop  autoplay></lottie-player>
      <input type="text" placeholder="full name" name="txt_full_name" value='<?php echo $full_name ?>'/>
      <input type="email" placeholder="email address" name="txt_email" value='<?php echo $email ?>'/>
      <input type="text" placeholder="pick a username" name="txt_user_name" value='<?php echo $user_name ?>'/>
      <input type="date" name="txt_date_of_birth"/>
      <input type="password" placeholder="set a password" name="txt_password" value='<?php echo $password ?>'/>
      <input type="password" placeholder="confirm password" name="txt_re_password" value='<?php echo $re_password ?>'/>
      <span class="message" style="color: indianred; font-weight: bold;"><?php echo $message; ?></span>
      <button name="btn_sign_up">SIGN UP</button>
    </form>

    <form class="login-form" method="POST">
     
    </form>
  </div>
</div>

</body>
</html>

