package com.doctorblue.qlvt.ui.home

import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.doctorblue.qlvt.R
import com.doctorblue.qlvt.data.Kho
import com.doctorblue.qlvt.databinding.FragmentKhoBinding
import com.doctorblue.qlvt.ui.viewmodel.KhoViewModel
import com.doctorblue.qlvt.utils.viewLifecycleLazy

class KhoFragment : Fragment(R.layout.fragment_kho) {

    private val binding by viewLifecycleLazy {
        FragmentKhoBinding.bind(requireView())
    }

    private val khoViewModel: KhoViewModel by lazy {
        ViewModelProvider(
            this,
            KhoViewModel.KhoViewModelFactory(requireNotNull(this.activity).application)
        )[(KhoViewModel::class.java)]
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.btnAdd.setOnClickListener {

            val tenKho = binding.edtTenKho.text.toString()

            if (inputCheck(tenKho)) {
                val kho = Kho(tenKho)
                khoViewModel.themKho(kho)
                Toast.makeText(
                    requireContext(),
                    "Thêm kho thành công",
                    Toast.LENGTH_LONG
                )
                    .show()
            } else {
                Toast.makeText(
                    requireContext(),
                    "Vui lòng điền tên kho",
                    Toast.LENGTH_LONG
                ).show()
            }
        }

    }
    private fun inputCheck(tenHangSX: String): Boolean {
        if (TextUtils.isEmpty(tenHangSX)) return false
        return true
    }
}

