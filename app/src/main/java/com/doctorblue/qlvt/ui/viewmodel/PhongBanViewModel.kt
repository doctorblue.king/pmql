package com.doctorblue.qlvt.ui.viewmodel

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.doctorblue.qlvt.data.PhongBan
import com.doctorblue.qlvt.data.repository.PhongBanRepository
import kotlinx.coroutines.launch

class PhongBanViewModel(application: Application) : ViewModel()  {
    private val phongBanRepository = PhongBanRepository(application)

    fun themPhongBan(phongBan: PhongBan) = viewModelScope.launch {
        phongBanRepository.themPhongBan(phongBan)
    }

    fun suaPhongBan(phongBan: PhongBan) = viewModelScope.launch {
        phongBanRepository.suaPhongBan(phongBan)
    }

    fun xoaPhongBan(phongBan: PhongBan) = viewModelScope.launch {
        phongBanRepository.xoaPhongBan(phongBan)
    }

    fun laytatCaPhongBan() = phongBanRepository.laytatCaPhongBan()


    class PhongBanViewModelFactory(private val application: Application) : ViewModelProvider.Factory {

        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(PhongBanViewModel::class.java)) {
                @Suppress("UNCHECKED_CAST")
                return PhongBanViewModel(application) as T
            }

            throw IllegalArgumentException("Unable construct viewModel")
        }

    }
}