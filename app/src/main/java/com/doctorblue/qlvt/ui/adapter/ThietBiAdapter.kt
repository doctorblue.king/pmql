package com.doctorblue.qlvt.ui.adapter

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.doctorblue.qlvt.R
import com.doctorblue.qlvt.data.ThietBi

class ThietBiAdapter(
    private val onLongClick: ((ThietBi, LinearLayout) -> Unit)?,
    private val onClick: (ThietBi) -> Unit
) :
    RecyclerView.Adapter<ThietBiAdapter.ThietBiViewHolder>() {
    private var thietBis: List<ThietBi> = listOf()
    private var thanhLys: List<ThietBi> = listOf()


    inner class ThietBiViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val txtSeri: TextView = itemView.findViewById(R.id.txt_so_thiet_bi)
        private val txtTenTB: TextView = itemView.findViewById(R.id.txt_ten_thiet_bi)
        private val txtGioiHan: TextView = itemView.findViewById(R.id.txt_gioi_han_su_dung)
        private val txtTinhTrang: TextView = itemView.findViewById(R.id.txt_tinh_trang_su_dung)
        private val txtNgaySD: TextView = itemView.findViewById(R.id.txt_ngay_su_dung)
        private val txtMaPhongBan: TextView = itemView.findViewById(R.id.txt_ma_phong_ban)
        private val layoutTB: LinearLayout = itemView.findViewById(R.id.layout_item_tb)

        fun onBind(thietBi: ThietBi) {
            txtSeri.text = ("Seri : ${thietBi.seri}")
            txtTenTB.text = ("Ten thiet bi : ${thietBi.tenThietBi}")
            txtGioiHan.text = ("Giới hạn : ${thietBi.gioiHanSuDung} giờ")
            txtTinhTrang.text = ("Tình trạng : ${thietBi.tinhTrangSuDung}%")
            txtNgaySD.text =
                if (thietBi.ngayDuaVaoSuDung.isEmpty()) "trong kho" else ("Ngày đưa vào sd : ${thietBi.ngayDuaVaoSuDung}")
            txtMaPhongBan.text = ("Mã phòng : ${thietBi.maPhongBan}")

            if (thanhLys.find { tb -> tb.seri == thietBi.seri } != null) {
                layoutTB.setBackgroundColor(Color.parseColor("#FF6CE0EF"))
            } else {
                layoutTB.setBackgroundColor(Color.parseColor("#FFFFFF"))
            }

            layoutTB.setOnClickListener {
                onClick(thietBi)
            }
            onLongClick?.let { onLong ->
                layoutTB.setOnLongClickListener {
                    onLong(thietBi, layoutTB)
                    true
                }
            }

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ThietBiViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.items_thiet_bi, parent, false)
        return ThietBiViewHolder((view))
    }

    override fun onBindViewHolder(holder: ThietBiViewHolder, position: Int) {
        holder.onBind(thietBis[position])
    }

    override fun getItemCount(): Int = thietBis.size

    fun setList(thietBis: List<ThietBi>) {
        this.thietBis = thietBis
        notifyDataSetChanged()
    }

    fun getList() = thietBis

    fun setThanhLys(thietBis: List<ThietBi>){
        this.thanhLys = thietBis
        notifyDataSetChanged()
    }
}