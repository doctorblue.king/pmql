package com.doctorblue.qlvt.ui.home

import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.doctorblue.qlvt.R
import com.doctorblue.qlvt.data.LoaiThietBi
import com.doctorblue.qlvt.databinding.FragmentLoaiThietBiBinding
import com.doctorblue.qlvt.ui.viewmodel.LoaiThietBiViewModel

class LoaiThietBiFragment : Fragment(R.layout.fragment_loai_thiet_bi) {

    private val binding by lazy {
        FragmentLoaiThietBiBinding.bind(requireView())
    }

    private val loaiThietbiViewModel: LoaiThietBiViewModel by lazy {
        ViewModelProvider(
            this,
            LoaiThietBiViewModel.LoaiViewModelFactory(requireNotNull(this.activity).application)
        )[(LoaiThietBiViewModel::class.java)]
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.btnAdd.setOnClickListener {

            val tenThietBi = binding.edtTenThietBi.text.toString()
            val moTa = binding.edtMoTa.text.toString()

            if (inputCheck(tenThietBi, moTa)) {
                val loaiThietBi = LoaiThietBi(
                    tenThietBi,
                    moTa
                )
                loaiThietbiViewModel.themLoaiThietBi(loaiThietBi)
                Toast.makeText(requireContext(), "Thêm loại thiết bị thành công", Toast.LENGTH_LONG)
                    .show()
            } else {
                Toast.makeText(
                    requireContext(),
                    "Vui lòng điền vào tất cả các giá trị",
                    Toast.LENGTH_LONG
                ).show()
            }
        }
    }

    private fun inputCheck(tenThietBi: String, moTa: String): Boolean {
        if (TextUtils.isEmpty(tenThietBi) && TextUtils.isEmpty(moTa)) return false
        else if (TextUtils.isEmpty(tenThietBi) || TextUtils.isEmpty(moTa)) return false
        return true
    }
}