package com.doctorblue.qlvt.ui.viewmodel

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.doctorblue.qlvt.data.LoaiThietBi
import com.doctorblue.qlvt.data.repository.LoaiThietBiRepository
import kotlinx.coroutines.launch

class LoaiThietBiViewModel(application: Application) : ViewModel() {
    private val loaiThietBiRepository = LoaiThietBiRepository(application)

    fun themLoaiThietBi(loaiThietBi: LoaiThietBi) = viewModelScope.launch {
        loaiThietBiRepository.themLoaiThietBi(loaiThietBi)
    }

    fun suaLoaiThietBi(loaiThietBi: LoaiThietBi) = viewModelScope.launch {
        loaiThietBiRepository.suaLoaiThietBi(loaiThietBi)
    }

    fun xoaLoaiThietBi(loaiThietBi: LoaiThietBi) = viewModelScope.launch {
        loaiThietBiRepository.xoaLoaiThietBi(loaiThietBi)
    }

    fun layTatCaLoaiThietBi() = loaiThietBiRepository.layTatCaLoaiThietBi()


    class LoaiViewModelFactory(private val application: Application) : ViewModelProvider.Factory {

        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(LoaiThietBiViewModel::class.java)) {
                @Suppress("UNCHECKED_CAST")
                return LoaiThietBiViewModel(application) as T
            }

            throw IllegalArgumentException("Unable construct viewModel")
        }

    }
}