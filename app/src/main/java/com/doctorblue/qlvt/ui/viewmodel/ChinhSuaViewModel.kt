package com.doctorblue.qlvt.ui.viewmodel

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.doctorblue.qlvt.data.ChiTietChinhSua
import com.doctorblue.qlvt.data.repository.ChinhSuaRepository
import kotlinx.coroutines.launch

class ChinhSuaViewModel(application: Application) : ViewModel()  {
    private val chinhSuaRepository = ChinhSuaRepository(application)

    fun themChinhSua(chinhSua: ChiTietChinhSua) = viewModelScope.launch {
        chinhSuaRepository.themChinhSua(chinhSua)
    }

    fun suaChinhSua(chinhSua: ChiTietChinhSua) = viewModelScope.launch {
        chinhSuaRepository.suaChinhSua(chinhSua)
    }
    fun xoaChinhSua(chinhSua: ChiTietChinhSua) = viewModelScope.launch {
        chinhSuaRepository.xoaChinhSua(chinhSua)
    }
    fun layTatCaChinhSua(chinhSua: ChiTietChinhSua) = chinhSuaRepository.layTatCaChinhSua()

    class ChinhSuaViewModelFactory(private val application: Application) : ViewModelProvider.Factory {

        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(ChinhSuaViewModel::class.java)) {
                @Suppress("UNCHECKED_CAST")
                return ChinhSuaViewModel(application) as T
            }

            throw IllegalArgumentException("Unable construct viewModel")
        }

    }
}