package com.doctorblue.qlvt.ui.home

import android.app.DatePickerDialog
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.doctorblue.qlvt.R
import com.doctorblue.qlvt.data.ChiTietChinhSua
import com.doctorblue.qlvt.data.ThietBi
import com.doctorblue.qlvt.ui.viewmodel.ChinhSuaViewModel
import com.doctorblue.qlvt.ui.viewmodel.ThietBiViewModel
import com.doctorblue.qlvt.utils.TrangThaiTB
import kotlinx.android.synthetic.main.fragment_chi_tiet_chinh_sua.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.round

class FragmentChiTietChinhSua : Fragment(R.layout.fragment_chi_tiet_chinh_sua) {

    private var nkt = ""
    private val simpleDateFormat = SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH)
    private val c: Calendar = Calendar.getInstance()
    private val mYear = c.get(Calendar.YEAR)
    private val mMonth = c.get(Calendar.MONTH)
    private val mDay = c.get(Calendar.DAY_OF_MONTH)
    private var trangThaiTB = TrangThaiTB.BINH_THUONG
    private var dsThietBi: List<ThietBi>? = null
    private var idThietBi = -1
    private var thietBi: ThietBi? = null

    private val chinhSuaViewModel by lazy {
        ViewModelProvider(
            this,
            ChinhSuaViewModel.ChinhSuaViewModelFactory(requireActivity().application)
        )[ChinhSuaViewModel::class.java]
    }
    private val thietBiViewModel by lazy {
        ViewModelProvider(
            this,
            ThietBiViewModel.ThietBiViewModelFactory(requireActivity().application)
        )[ThietBiViewModel::class.java]
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initControls()
        initEvents()
    }

    private fun initControls() {
        layThietBi()
    }

    private fun initEvents() {
        txt_ngay_kiem_tra.setOnClickListener {
            chonNCS()
        }
        sp_ma_thiet_bi.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                idThietBi = dsThietBi!![position].seri
                thietBi = dsThietBi!![position]
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

        }

        rgr_trang_thai.setOnCheckedChangeListener { group, checkedId ->
            when (checkedId) {
                R.id.rb_binh_thuong -> {
                    trangThaiTB = TrangThaiTB.BINH_THUONG
                }
                R.id.rb_sua_chua -> {
                    trangThaiTB = TrangThaiTB.HONG
                }
                R.id.rb_thay_the -> {
                    trangThaiTB = TrangThaiTB.THAY_THE
                }
            }
        }
        btn_Add.setOnClickListener {
            if (nkt.isEmpty()) {
                Toast.makeText(
                    requireContext(),
                    "Bạn chưa chọn ngày kiểm tra !!!",
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                if (idThietBi == -1) {
                    Toast.makeText(
                        requireContext(),
                        "Bạn chưa chọn thiết bị !!!",
                        Toast.LENGTH_SHORT
                    ).show()
                } else {
                    var tinhTrangSuDung: Double
                    if (thietBi!!.ngayDuaVaoSuDung.isEmpty()) {
                        Toast.makeText(
                            requireContext(),
                            "Thiết bị này chưa được đưa vào sử dụng không thể chỉnh sửa !",
                            Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        val dateNgaySuDung: Date =
                            simpleDateFormat.parse(thietBi!!.ngayDuaVaoSuDung)!!
                        val dateNgayChinhSua: Date = simpleDateFormat.parse(nkt)!!

                        tinhTrangSuDung = when (trangThaiTB) {
                            TrangThaiTB.BINH_THUONG -> {
                                100 - (daysBetween(
                                    dateNgaySuDung,
                                    dateNgayChinhSua
                                ) * 8.0 / thietBi!!.gioiHanSuDung * 100)
                            }
                            TrangThaiTB.HONG -> {
                                100 - (daysBetween(
                                    dateNgaySuDung,
                                    dateNgayChinhSua
                                ) * 8.0 / thietBi!!.gioiHanSuDung * 100)
                            }
                            TrangThaiTB.THAY_THE -> {
                                0.0
                            }
                        }
                        tinhTrangSuDung = round(tinhTrangSuDung * 100) / 100
                        val chitet = edt_chi_tiet.text.toString()
                        if (trangThaiTB != TrangThaiTB.BINH_THUONG) {
                            if (chitet.isEmpty()) {
                                Toast.makeText(
                                    requireContext(),
                                    "Chi tiết thiết bị hiện đang trống hãy cho biết thiết bị có vấn đề ở đâu",
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        } else {
                            val chiTietChinhSua = ChiTietChinhSua(
                                idThietBi,
                                trangThaiTB.toString(),
                                chitet,
                                nkt
                            )
                            chinhSuaViewModel.themChinhSua(chiTietChinhSua)
                            thietBi!!.tinhTrangSuDung = tinhTrangSuDung
                            thietBiViewModel.suaThietBi(thietBi!!)
                            Toast.makeText(
                                requireContext(),
                                "Thêm thành công !",
                                Toast.LENGTH_SHORT
                            ).show()
                        }

                    }
                }
            }
        }
    }

    fun daysBetween(d1: Date, d2: Date): Int {
        return ((d2.time - d1.time) / (1000 * 60 * 60 * 24)).toInt()
    }

    private fun chonNCS() {
        val datePickerDialog = DatePickerDialog(
            requireContext(),
            { _, year, monthOfYear, dayOfMonth ->
                c.set(Calendar.YEAR, year)
                c.set(Calendar.MONTH, monthOfYear)
                c.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                nkt = simpleDateFormat.format(c.time)

                txt_ngay_kiem_tra.text = nkt
            },
            mYear,
            mMonth,
            mDay
        )
        datePickerDialog.show()
    }

    private fun layThietBi() {
        thietBiViewModel.layTatCaThietBi().observe(viewLifecycleOwner, {
            val aa: ArrayAdapter<*> =
                ArrayAdapter<Any?>(requireContext(), android.R.layout.simple_spinner_item, it)
            aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            sp_ma_thiet_bi.adapter = aa
            dsThietBi = it
        })
    }
}