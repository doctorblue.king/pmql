package com.doctorblue.qlvt.ui.home

import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.doctorblue.qlvt.R
import com.doctorblue.qlvt.data.HangSanXuat
import com.doctorblue.qlvt.data.PhongBan
import com.doctorblue.qlvt.data.ThietBi
import com.doctorblue.qlvt.ui.adapter.ThietBiAdapter
import com.doctorblue.qlvt.ui.viewmodel.HangSXViewModel
import com.doctorblue.qlvt.ui.viewmodel.PhongBanViewModel
import com.doctorblue.qlvt.ui.viewmodel.ThietBiViewModel
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.launch

class HomeFragment : Fragment(R.layout.fragment_home) {

    private val thietBiViewModel by lazy {
        ViewModelProvider(
            this,
            ThietBiViewModel.ThietBiViewModelFactory(requireActivity().application)
        )[ThietBiViewModel::class.java]
    }
    private val adapter: ThietBiAdapter by lazy {
        ThietBiAdapter(null, onItemClick)
    }
    private val phongBanViewModel: PhongBanViewModel by lazy {
        ViewModelProvider(
            this,
            PhongBanViewModel.PhongBanViewModelFactory(requireNotNull(this.activity).application)
        )[(PhongBanViewModel::class.java)]
    }
    private val hangSXViewModel by lazy {
        ViewModelProvider(
            this,
            HangSXViewModel.HangSXModelFactory(requireActivity().application)
        )[HangSXViewModel::class.java]
    }

    private fun layHangSX() {
        hangSXViewModel.layTatCaHangSanXuat().observe(viewLifecycleOwner, {
            val aa: ArrayAdapter<*> =
                ArrayAdapter<Any?>(requireContext(), android.R.layout.simple_spinner_item, it)
            aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            sp_hang_sx.adapter = aa
            dsHangSX = it
        })
    }

    private var dsHangSX: List<HangSanXuat>? = null
    private var dsPhongBan: List<PhongBan>? = null
    lateinit var dsThietBi: List<ThietBi>
    private var idPhong = -1
    private var idHang = -1

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        addControls()
        addEvents()
    }

    private fun addControls() {
        layPhong()
        layHangSX()

        rv_home.layoutManager = LinearLayoutManager(requireContext())
        rv_home.setHasFixedSize(true)
        rv_home.adapter = adapter
        thietBiViewModel.layTatCaThietBi().observe(viewLifecycleOwner, {
            dsThietBi = it
            adapter.setList(it)
        })
        if (cb_show.isChecked) {
            loc()
        }
    }

    private fun addEvents() {
        sp_hang_sx.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                idHang = dsHangSX!![position].idHang
                if (cb_show.isChecked) {
                    loc()
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

        }
        sp_ma_phong_ban.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                idPhong = dsPhongBan!![position].id
                if (cb_show.isChecked) {
                    loc()
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

        }
        cb_show.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                layout_bo_loc.visibility = View.VISIBLE
                loc()
            } else {
                layout_bo_loc.visibility = View.GONE
                adapter.setList(dsThietBi)
            }
        }

    }

    private val onItemClick: (ThietBi) -> Unit = {
        val bundle = bundleOf(
            "THIET_BI" to it
        )
        findNavController().navigate(R.id.action_nav_home_to_fragmentChinhSua, bundle)
    }

    private fun layPhong() {
        phongBanViewModel.laytatCaPhongBan().observe(viewLifecycleOwner, {
            val aa: ArrayAdapter<*> =
                ArrayAdapter<Any?>(requireContext(), android.R.layout.simple_spinner_item, it)
            aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            sp_ma_phong_ban.adapter = aa
            dsPhongBan = it
        })
    }

    private fun loc() {
        if (idPhong != -1) {
            if (dsThietBi.isNotEmpty()) {
                GlobalScope.launch(Dispatchers.Main) {
                    adapter.setList(
                        dsThietBi.asFlow().filter { thietBi -> thietBi.maPhongBan == idPhong }
                            .toList())
                    if (idHang != -1) {
                        adapter.setList(
                            adapter.getList().asFlow()
                                .filter { thietBi -> thietBi.idHangSX == idHang }
                                .toList())
                    }
                }
            }


        }
    }
}