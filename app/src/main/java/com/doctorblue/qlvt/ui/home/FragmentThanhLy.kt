package com.doctorblue.qlvt.ui.home

import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.LinearLayout
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.doctorblue.qlvt.R
import com.doctorblue.qlvt.data.HangSanXuat
import com.doctorblue.qlvt.data.PhongBan
import com.doctorblue.qlvt.data.ThietBi
import com.doctorblue.qlvt.ui.adapter.ThietBiAdapter
import com.doctorblue.qlvt.ui.viewmodel.PhongBanViewModel
import com.doctorblue.qlvt.ui.viewmodel.ThietBiViewModel
import kotlinx.android.synthetic.main.fragment_thanh_li.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.launch

class FragmentThanhLy : Fragment(R.layout.fragment_thanh_li) {

    private val thietBiViewModel by lazy {
        ViewModelProvider(
            this,
            ThietBiViewModel.ThietBiViewModelFactory(requireActivity().application)
        )[ThietBiViewModel::class.java]
    }

    private val adapter: ThietBiAdapter by lazy {
        ThietBiAdapter(onItemLongClick, onItemClick)
    }

    private lateinit var dsThietBi: List<ThietBi>
    private var dsPhongBan: List<PhongBan>? = null
    private var idPhong = -1



    private val thanhLys: MutableList<ThietBi> = mutableListOf()

    private val phongBanViewModel: PhongBanViewModel by lazy {
        ViewModelProvider(
            this,
            PhongBanViewModel.PhongBanViewModelFactory(requireNotNull(this.activity).application)
        )[(PhongBanViewModel::class.java)]
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        layPhong()

        rv_ds_thiet_bi.layoutManager = LinearLayoutManager(requireContext())
        rv_ds_thiet_bi.setHasFixedSize(true)
        rv_ds_thiet_bi.adapter = adapter

        thietBiViewModel.layThietBiThanhLy().observe(viewLifecycleOwner, {
            adapter.setList(it)
            adapter.setThanhLys(thanhLys)
            dsThietBi = it
        })

        btn_Add.setOnClickListener {
            if (thanhLys.size == 0) {
                Toast.makeText(
                    requireContext(),
                    "Danh sách thanh lý đang trống !!",
                    Toast.LENGTH_LONG
                ).show()
            } else {
                for (item in thanhLys) {
                    thietBiViewModel.xoaThietBi(item)
                }
                thanhLys.clear()
            }
        }
        sp_ma_phong_ban.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                idPhong = dsPhongBan!![position].id
                if (cb_show.isChecked){
                    loc()
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

        }
        cb_phu_kien.setOnCheckedChangeListener { _, _ ->
            if (cb_show.isChecked){
                loc()
            }
        }
        cb_show.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                layout_bo_loc.visibility = View.VISIBLE
                loc()
            }else {
                layout_bo_loc.visibility = View.GONE
                adapter.setList(dsThietBi)
                adapter.setThanhLys(thanhLys)
            }
        }
    }

    private val onItemClick: (ThietBi) -> Unit = {
    }


    private fun locTheoPhu(isPhuKien: Boolean) {
        if (dsThietBi.isNotEmpty()) {
            if (isPhuKien) {
                GlobalScope.launch(Dispatchers.Main) {
                    adapter.setList(
                        adapter.getList().asFlow().filter { thietBi -> thietBi.idTB != -1 }
                            .toList())
                    adapter.setThanhLys(thanhLys)
                }
            } else {
                GlobalScope.launch(Dispatchers.Main) {
                    adapter.setList(
                        adapter.getList().asFlow().filter { thietBi -> thietBi.idTB == -1 }
                            .toList())
                    adapter.setThanhLys(thanhLys)

                }
            }
        }
    }

    private fun loc() {
        if (idPhong != -1) {
            if (dsThietBi.isNotEmpty()) {
                GlobalScope.launch(Dispatchers.Main) {
                    adapter.setList(
                        dsThietBi.asFlow().filter { thietBi -> thietBi.maPhongBan == idPhong }
                            .toList())
                    adapter.setThanhLys(thanhLys)

                    locTheoPhu(cb_phu_kien.isChecked)
                }
            }


        }
    }


    private val onItemLongClick: (ThietBi, LinearLayout) -> Unit = { tb, layout ->
        if (thanhLys.find { thietBi -> thietBi.seri == tb.seri } != null) {
            layout.setBackgroundColor(Color.parseColor("#FFFFFF"))
            thanhLys.remove(tb)
        } else {
            layout.setBackgroundColor(Color.parseColor("#FF6CE0EF"))
            thanhLys.add(tb)
        }
        btn_Add.text = ("Thanh lý (${thanhLys.size})")
    }

    private fun layPhong() {
        phongBanViewModel.laytatCaPhongBan().observe(viewLifecycleOwner, {
            val aa: ArrayAdapter<*> =
                ArrayAdapter<Any?>(requireContext(), android.R.layout.simple_spinner_item, it)
            aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            sp_ma_phong_ban.adapter = aa
            dsPhongBan = it
        })
    }

}