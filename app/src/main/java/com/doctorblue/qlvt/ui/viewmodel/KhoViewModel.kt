package com.doctorblue.qlvt.ui.viewmodel

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.doctorblue.qlvt.data.Kho
import com.doctorblue.qlvt.data.repository.KhoRepository
import kotlinx.coroutines.launch

class KhoViewModel(application: Application) : ViewModel() {
    private val khoRepository = KhoRepository(application)

    fun themKho(kho: Kho) = viewModelScope.launch {
        khoRepository.themKho(kho)
    }

    fun suaKho(kho: Kho) = viewModelScope.launch {
        khoRepository.suaKho(kho)
    }

    fun xoaKho(kho: Kho) = viewModelScope.launch {
        khoRepository.xoaKho(kho)
    }

    fun layTatCaKho() = khoRepository.layTatCaKho()


    class KhoViewModelFactory(private val application: Application) : ViewModelProvider.Factory {

        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(KhoViewModel::class.java)) {
                @Suppress("UNCHECKED_CAST")
                return KhoViewModel(application) as T
            }

            throw IllegalArgumentException("Unable construct viewModel")
        }

    }
}