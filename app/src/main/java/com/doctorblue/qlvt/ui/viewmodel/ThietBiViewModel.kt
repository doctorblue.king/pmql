package com.doctorblue.qlvt.ui.viewmodel

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.doctorblue.qlvt.data.ThietBi
import com.doctorblue.qlvt.data.repository.ThietBiRepository
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class ThietBiViewModel(application: Application) : ViewModel()  {
    private val thietBiRepository = ThietBiRepository(application)


     fun themThietBi(thietBi: ThietBi)= viewModelScope.launch {
         thietBiRepository.themThietBi(thietBi)
     }
     fun suaThietBi(thietBi: ThietBi) = viewModelScope.launch {
         thietBiRepository.suaThietBi(thietBi)
     }
     fun xoaThietBi(thietBi: ThietBi) = viewModelScope.launch {
         thietBiRepository.xoaThietBi(thietBi)
     }

    fun layTatCaThietBi(): LiveData<List<ThietBi>> = thietBiRepository.layTatCaThietBi()

    fun layThietBiTheoSoSeri(seri:Int): LiveData<ThietBi> = thietBiRepository.layThietBiTheoSoSeri(seri)

    fun layThietBiThanhLy(): LiveData<List<ThietBi>> = thietBiRepository.layThietBiThanhLy()


    class ThietBiViewModelFactory(private val application: Application) : ViewModelProvider.Factory {

        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(ThietBiViewModel::class.java)) {
                @Suppress("UNCHECKED_CAST")
                return ThietBiViewModel(application) as T
            }

            throw IllegalArgumentException("Unable construct viewModel")
        }

    }
}