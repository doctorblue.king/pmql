
package com.doctorblue.qlvt.ui.home

import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.doctorblue.qlvt.R
import com.doctorblue.qlvt.data.PhongBan
import com.doctorblue.qlvt.databinding.FragmentPhongBanBinding
import com.doctorblue.qlvt.ui.viewmodel.PhongBanViewModel

class PhongBanFragment : Fragment(R.layout.fragment_phong_ban) {

    private val binding by lazy {
        FragmentPhongBanBinding.bind(requireView())
    }

    private val phongBanViewModel: PhongBanViewModel by lazy {
        ViewModelProvider(
            this,
            PhongBanViewModel.PhongBanViewModelFactory(requireNotNull(this.activity).application)
        )[(PhongBanViewModel::class.java)]
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.btnAdd.setOnClickListener {
            val tenPhongBan = binding.edtTenPhongBan.text.toString()
            if (inputCheck(tenPhongBan)) {
                val phongBan = PhongBan(
                    tenPhongBan
                )
                phongBanViewModel.themPhongBan(phongBan)
                Toast.makeText(
                    requireContext(),
                    "Thêm dữ liệu thành công",
                    Toast.LENGTH_LONG
                ).show()
            } else {
                Toast.makeText(
                    requireContext(),
                    "Bạn chưa điền đầy đủ thông tin!",
                    Toast.LENGTH_LONG
                ).show()
            }

        }
    }

    private fun inputCheck(tenPhongBan: String): Boolean {
        return !TextUtils.isEmpty(tenPhongBan)
    }
}