package com.doctorblue.qlvt.ui.viewmodel

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.doctorblue.qlvt.data.HangSanXuat
import com.doctorblue.qlvt.data.Kho
import com.doctorblue.qlvt.data.repository.HangSanXuatRepository
import com.doctorblue.qlvt.database.VatTuDatabase
import kotlinx.coroutines.launch

class HangSXViewModel(application: Application) : ViewModel() {
    private val hangSanXuatRepository = HangSanXuatRepository(application)

    fun themHangSanXuat(hangSanXuat: HangSanXuat) = viewModelScope.launch {
        hangSanXuatRepository.themHangSanXuat(hangSanXuat)
    }

    fun suaHangSanXuat(hangSanXuat: HangSanXuat) = viewModelScope.launch {
        hangSanXuatRepository.suaHangSanXuat(hangSanXuat)
    }

    fun xoaHangSanXuat(hangSanXuat: HangSanXuat) = viewModelScope.launch {
        hangSanXuatRepository.xoaHangSanXuat(hangSanXuat)
    }

    fun layTatCaHangSanXuat() = hangSanXuatRepository.layTatCaHangSanXuat()

    class HangSXModelFactory(private val application: Application) : ViewModelProvider.Factory {

        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(HangSXViewModel::class.java)) {
                @Suppress("UNCHECKED_CAST")
                return HangSXViewModel(application) as T
            }

            throw IllegalArgumentException("Unable construct viewModel")
        }

    }
}