
package com.doctorblue.qlvt.ui.home

import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.doctorblue.qlvt.R
import com.doctorblue.qlvt.data.HangSanXuat
import com.doctorblue.qlvt.databinding.FragmentHangSanXuatBinding
import com.doctorblue.qlvt.ui.viewmodel.HangSXViewModel
import com.doctorblue.qlvt.utils.viewLifecycleLazy

class HangSanXuatFragment : Fragment(R.layout.fragment_hang_san_xuat) {

    private val binding by viewLifecycleLazy {
        FragmentHangSanXuatBinding.bind(requireView())
    }

    private val hangSanXuatViewModel: HangSXViewModel by lazy {
        ViewModelProvider(
            this,
            HangSXViewModel.HangSXModelFactory(requireNotNull(this.activity).application)
        )[(HangSXViewModel::class.java)]
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.btnAdd.setOnClickListener {

            val tenHangSX = binding.edtTenHangSx.text.toString()

            if (inputCheck(tenHangSX)) {
                val hangSX = HangSanXuat(tenHangSX)
                hangSanXuatViewModel.themHangSanXuat(hangSX)
                Toast.makeText(requireContext(), "Thêm hãng sản xuất thành công", Toast.LENGTH_LONG)
                    .show()
            } else {
                Toast.makeText(
                    requireContext(),
                    "Vui lòng điền hãng sản xuất",
                    Toast.LENGTH_LONG
                ).show()
            }
        }
    }

    private fun inputCheck(tenHangSX: String): Boolean {
        if (TextUtils.isEmpty(tenHangSX)) return false
        return true
    }

}