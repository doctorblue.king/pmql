package com.doctorblue.qlvt.ui.home

import android.app.DatePickerDialog
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.doctorblue.qlvt.R
import com.doctorblue.qlvt.data.HangSanXuat
import com.doctorblue.qlvt.data.LoaiThietBi
import com.doctorblue.qlvt.data.PhongBan
import com.doctorblue.qlvt.data.ThietBi
import com.doctorblue.qlvt.ui.viewmodel.HangSXViewModel
import com.doctorblue.qlvt.ui.viewmodel.LoaiThietBiViewModel
import com.doctorblue.qlvt.ui.viewmodel.PhongBanViewModel
import com.doctorblue.qlvt.ui.viewmodel.ThietBiViewModel
import kotlinx.android.synthetic.main.fragment_thiet_bi.*
import java.text.SimpleDateFormat
import java.util.*


class FragmentThietBi : Fragment(R.layout.fragment_thiet_bi) {

    private var nsd: String = ""
    private val simpleDateFormat = SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH)
    private var dsPhongBan: List<PhongBan>? = null
    private var dsLoaiTB: List<LoaiThietBi>? = null
    private var dsHangSX: List<HangSanXuat>? = null
    private var dsThietBi: List<ThietBi>? = null

    private var idPhong = -1
    private var idLoai = -1
    private var idHang = -1
    private var idThietBi = -1

    private val thietBiViewModel by lazy {
        ViewModelProvider(
            this,
            ThietBiViewModel.ThietBiViewModelFactory(requireActivity().application)
        )[ThietBiViewModel::class.java]
    }

    private val phongBanViewModel: PhongBanViewModel by lazy {
        ViewModelProvider(
            this,
            PhongBanViewModel.PhongBanViewModelFactory(requireNotNull(this.activity).application)
        )[(PhongBanViewModel::class.java)]
    }

    private val loaiThietbiViewModel: LoaiThietBiViewModel by lazy {
        ViewModelProvider(
            this,
            LoaiThietBiViewModel.LoaiViewModelFactory(requireNotNull(this.activity).application)
        )[(LoaiThietBiViewModel::class.java)]
    }

    private val hangSXViewModel by lazy {
        ViewModelProvider(
            this,
            HangSXViewModel.HangSXModelFactory(requireActivity().application)
        )[HangSXViewModel::class.java]
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initControls()
        initEvents()
    }

    private fun initEvents() {
        txt_ngay_su_dung.setOnClickListener {
            chonNSD()
        }
        sp_ma_phong_ban.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                idPhong = dsPhongBan!![position].id
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

        }

        sp_id_loai.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                idLoai = dsLoaiTB!![position].id
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

        }
        sp_hang_sx.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                idHang = dsHangSX!![position].idHang
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

        }
        sp_thiet_bi_khac.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                idThietBi = dsThietBi!![position].seri
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

        }


        btn_Add.setOnClickListener {
            themThietBi()
        }
        cb_phu_kien.setOnCheckedChangeListener { _, isChecked ->
            if (!isChecked) {
                layout_tb_khac.visibility = View.GONE
                idThietBi = -1
            } else {
                layout_tb_khac.visibility = View.VISIBLE

            }
        }


    }

    private fun initControls() {
        layLoaiThietBi()
        layPhong()
        layHangSX()
        layThietBi()
    }

    private fun themThietBi() {
        if (checkEmpty()) {
            Toast.makeText(
                requireContext(),
                "Bạn chưa điền đầy đủ thông tin mời kiêm tra lại !!!",
                Toast.LENGTH_LONG
            )
                .show()
        } else {
            val thietBi = ThietBi(
                edt_ten_thiet_bi.text.toString(),
                idLoai,
                idHang,
                edt_gioi_han_su_dung.text.toString().toLong(),
                100.0,
                edt_thong_tin_khac.text.toString(),
                nsd,
                idPhong,
                idThietBi
            )
            idThietBi = -1
            cb_phu_kien.isChecked = false
            thietBiViewModel.themThietBi(thietBi)
            Toast.makeText(
                requireContext(),
                "Thêm thành công !!!",
                Toast.LENGTH_LONG
            ).show()

            edt_gioi_han_su_dung.setText("")
            edt_ten_thiet_bi.setText("")
            edt_thong_tin_khac.setText("")
            txt_ngay_su_dung.text = ("")
            edt_tinh_trang.setText("")
            nsd = ""
        }
    }

    private fun checkEmpty() =
        TextUtils.isEmpty(edt_ten_thiet_bi.text) || TextUtils.isEmpty(
            edt_gioi_han_su_dung.text
        ) || TextUtils.isEmpty(edt_ten_thiet_bi.text) || idLoai == -1 || idPhong == -1 || idHang == -1

    private fun chonNSD() {
        val c: Calendar = Calendar.getInstance()
        val mYear = c.get(Calendar.YEAR)
        val mMonth = c.get(Calendar.MONTH)
        val mDay = c.get(Calendar.DAY_OF_MONTH)


        val datePickerDialog = DatePickerDialog(
            requireContext(),
            { _, year, monthOfYear, dayOfMonth ->
                c.set(Calendar.YEAR, year)
                c.set(Calendar.MONTH, monthOfYear)
                c.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                nsd = simpleDateFormat.format(c.time)

                txt_ngay_su_dung.text =
                    (dayOfMonth.toString() + "/" + (monthOfYear + 1) + "/" + year)
            },
            mYear,
            mMonth,
            mDay
        )
        datePickerDialog.show()
    }


    private fun layLoaiThietBi() {
        loaiThietbiViewModel.layTatCaLoaiThietBi().observe(viewLifecycleOwner, {
            val aa: ArrayAdapter<*> =
                ArrayAdapter<Any?>(requireContext(), android.R.layout.simple_spinner_item, it)
            aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            sp_id_loai.adapter = aa
            dsLoaiTB = it
        })
    }

    private fun layPhong() {
        phongBanViewModel.laytatCaPhongBan().observe(viewLifecycleOwner, {
            val aa: ArrayAdapter<*> =
                ArrayAdapter<Any?>(requireContext(), android.R.layout.simple_spinner_item, it)
            aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            sp_ma_phong_ban.adapter = aa
            dsPhongBan = it
        })
    }

    private fun layHangSX() {
        hangSXViewModel.layTatCaHangSanXuat().observe(viewLifecycleOwner, {
            val aa: ArrayAdapter<*> =
                ArrayAdapter<Any?>(requireContext(), android.R.layout.simple_spinner_item, it)
            aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            sp_hang_sx.adapter = aa
            dsHangSX = it
        })
    }

    private fun layThietBi() {
        thietBiViewModel.layTatCaThietBi().observe(viewLifecycleOwner, {
            val aa: ArrayAdapter<*> =
                ArrayAdapter<Any?>(requireContext(), android.R.layout.simple_spinner_item, it)
            aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            sp_thiet_bi_khac.adapter = aa
            dsThietBi = it
        })
    }
}