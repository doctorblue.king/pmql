package com.doctorblue.qlvt.data.repository

import android.app.Application
import androidx.lifecycle.LiveData
import com.doctorblue.qlvt.data.PhongBan
import com.doctorblue.qlvt.database.VatTuDatabase
import com.doctorblue.qlvt.database.dao.VatTuDao

class PhongBanRepository(application: Application) {
    private val vatTuDao: VatTuDao

    init {
        val database: VatTuDatabase = VatTuDatabase.getInstance(application)
        vatTuDao = database.getVatTuDao()
    }

    suspend fun themPhongBan(phongBan: PhongBan) = vatTuDao.themPhongBan(phongBan)

    suspend fun suaPhongBan(phongBan: PhongBan) = vatTuDao.suaPhongBan(phongBan)

    suspend fun xoaPhongBan(phongBan: PhongBan) = vatTuDao.xoaPhongBan(phongBan)

    fun laytatCaPhongBan():LiveData<List<PhongBan>> = vatTuDao.layTatCaPhongBan()
}