package com.doctorblue.qlvt.data

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "tb_chinh_sua")
class ChiTietChinhSua(
        var seriThietBi: Int,
        var loaiTrangThai: String,
        var chiTiet: String,
        var ngayKiemTra: String,
) {
    @PrimaryKey(autoGenerate = true)
    var maChinhSua: Int = 0
    /**
     *  - Chi tiết chỉnh sửa
    + Mã chi tiết chỉnh sửa
    + Mã thiết bị
    + Loại trạng thái ( BÌNH THƯỜNG , SỬA , THAY THẾ )
    + Chi tiết ( Lỗi ở đâu, cần sửa những gì?, hay thay những linh kiện nào ... )
    + Ngày kiểm tra
     */
}