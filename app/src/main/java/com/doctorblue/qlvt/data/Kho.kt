package com.doctorblue.qlvt.data

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "tb_kho")
class Kho(var tenKho: String) {
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0
}