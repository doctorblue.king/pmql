package com.doctorblue.qlvt.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity(tableName = "tb_thiet_bi")
class ThietBi(
    var tenThietBi: String,
    var idloai: Int,
    var idHangSX: Int,
    var gioiHanSuDung: Long,
    var tinhTrangSuDung: Double,
    var thongTinKhac: String = "",
    var ngayDuaVaoSuDung: String,
    var maPhongBan: Int,
    var idTB: Int = 0
) :Serializable {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "seri")
    var seri: Int = 0
    override fun toString(): String {
        return "$tenThietBi (seri=$seri)"
    }
    /**
     *  - Thiết bị
    + Số seri thiết bị
    + Tên Thiết bị
    + Loại
    + Hãng sản xuất
    + Giới hạn sử dụng
    + Tình trạng sử dụng (%)
    + Thông tin khác ( nếu có )
    + Ngày đưa vào sử dụng
    + Mã phòng ban
     */

}