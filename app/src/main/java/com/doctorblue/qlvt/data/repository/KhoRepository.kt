package com.doctorblue.qlvt.data.repository

import android.app.Application
import com.doctorblue.qlvt.data.Kho
import com.doctorblue.qlvt.database.VatTuDatabase
import com.doctorblue.qlvt.database.dao.VatTuDao

class KhoRepository(application: Application) {
    private val vatTuDao: VatTuDao

    init {
        val database: VatTuDatabase = VatTuDatabase.getInstance(application)
        vatTuDao = database.getVatTuDao()
    }

    suspend fun themKho(kho: Kho) = vatTuDao.themKho(kho)

    suspend fun suaKho(kho: Kho) = vatTuDao.suaKho(kho)

    suspend fun xoaKho(kho: Kho) = vatTuDao.xoaKho(kho)

    fun layTatCaKho() = vatTuDao.layTatCaKho()

}