package com.doctorblue.qlvt.data.repository

import android.app.Application
import com.doctorblue.qlvt.data.LoaiThietBi
import com.doctorblue.qlvt.database.VatTuDatabase
import com.doctorblue.qlvt.database.dao.VatTuDao

class LoaiThietBiRepository(application: Application) {
    private val vatTuDao: VatTuDao

    init {
        val database: VatTuDatabase = VatTuDatabase.getInstance(application)
        vatTuDao = database.getVatTuDao()
    }

    suspend fun themLoaiThietBi(loaiThietBi: LoaiThietBi) = vatTuDao.themLoaiThietBi(loaiThietBi)

    suspend fun suaLoaiThietBi(loaiThietBi: LoaiThietBi) = vatTuDao.suaLoaiThietBi(loaiThietBi)

    suspend fun xoaLoaiThietBi(loaiThietBi: LoaiThietBi) = vatTuDao.xoaLoaiThietBi(loaiThietBi)

    fun layTatCaLoaiThietBi() = vatTuDao.layTatCaLoaiThietBi()

}