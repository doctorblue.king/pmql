package com.doctorblue.qlvt.data.repository

import android.app.Application
import androidx.lifecycle.LiveData
import com.doctorblue.qlvt.data.ThietBi
import com.doctorblue.qlvt.database.VatTuDatabase
import com.doctorblue.qlvt.database.dao.VatTuDao

class ThietBiRepository(application: Application) {
    private val vatTuDao: VatTuDao

    init {
        val database: VatTuDatabase = VatTuDatabase.getInstance(application)
        vatTuDao = database.getVatTuDao()
    }

    suspend fun themThietBi(thietBi: ThietBi) = vatTuDao.themThietBi(thietBi)
    suspend fun suaThietBi(thietBi: ThietBi) = vatTuDao.suaThietBi(thietBi)
    suspend fun xoaThietBi(thietBi: ThietBi) = vatTuDao.xoaThietBi(thietBi)

    fun layTatCaThietBi(): LiveData<List<ThietBi>> = vatTuDao.layTatCaThietBi()

    fun layThietBiTheoSoSeri(seri: Int): LiveData<ThietBi> = vatTuDao.layThietBiTheoSeri(seri)

    fun layThietBiThanhLy(): LiveData<List<ThietBi>> = vatTuDao.layThietBiThanhLy()
}