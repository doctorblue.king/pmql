package com.doctorblue.qlvt.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "tb_phong")
class PhongBan(
    @ColumnInfo(name = "ten_phong_ban")
    var tenPhongBan: String = ""
) {

    @PrimaryKey(autoGenerate = true)
    var id: Int = 0
    override fun toString(): String {
        return tenPhongBan
    }


}