package com.doctorblue.qlvt.data.repository

import android.app.Application
import com.doctorblue.qlvt.data.ChiTietChinhSua
import com.doctorblue.qlvt.database.VatTuDatabase
import com.doctorblue.qlvt.database.dao.VatTuDao

class ChinhSuaRepository(application: Application) {
    private val vatTuDao: VatTuDao

    init {
        val database: VatTuDatabase = VatTuDatabase.getInstance(application)
        vatTuDao = database.getVatTuDao()
    }
    suspend fun themChinhSua(chinhSua: ChiTietChinhSua) = vatTuDao.themChinhSua(chinhSua)

    suspend fun suaChinhSua(chinhSua: ChiTietChinhSua) = vatTuDao.suaChinhSua(chinhSua)

    suspend fun xoaChinhSua(chinhSua: ChiTietChinhSua) = vatTuDao.xoaChinhSua(chinhSua)

    fun layTatCaChinhSua() = vatTuDao.layTatCaChinhSua()


}