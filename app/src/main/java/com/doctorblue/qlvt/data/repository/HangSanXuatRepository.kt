package com.doctorblue.qlvt.data.repository

import android.app.Application
import com.doctorblue.qlvt.data.HangSanXuat
import com.doctorblue.qlvt.data.Kho
import com.doctorblue.qlvt.database.VatTuDatabase
import com.doctorblue.qlvt.database.dao.VatTuDao

class HangSanXuatRepository(application: Application) {
    private val vatTuDao: VatTuDao

    init {
        val database: VatTuDatabase = VatTuDatabase.getInstance(application)
        vatTuDao = database.getVatTuDao()
    }

    suspend fun themHangSanXuat(hangSanXuat: HangSanXuat) = vatTuDao.themHangSX(hangSanXuat)

    suspend fun suaHangSanXuat(hangSanXuat: HangSanXuat) = vatTuDao.suaHangSX(hangSanXuat)

    suspend fun xoaHangSanXuat(hangSanXuat: HangSanXuat) = vatTuDao.xoaHangSX(hangSanXuat)

    fun layTatCaHangSanXuat() = vatTuDao.layTatCaHangSanXuat()
}