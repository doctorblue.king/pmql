package com.doctorblue.qlvt.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "tb_hang_sx")
class HangSanXuat(

    @ColumnInfo(name = "ten_hang")
    var tenHang: String

    ) {

    @PrimaryKey(autoGenerate = true)
    var idHang: Int = 0

    override fun toString(): String {
        return tenHang
    }

}