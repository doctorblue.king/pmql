package com.doctorblue.qlvt.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "tb_loai_tb")
class LoaiThietBi(

    @ColumnInfo(name = "ten_loai")
    var tenLoai: String,

    @ColumnInfo(name = "mo_ta")
    var mota: String
) {
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0
    override fun toString(): String {
        return "$tenLoai-(${mota.substring(5)} ...)"
    }
}