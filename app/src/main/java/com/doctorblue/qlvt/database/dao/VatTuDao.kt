package com.doctorblue.qlvt.database.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.doctorblue.qlvt.data.*

@Dao
interface VatTuDao {
    //Thiet Bi
    @Insert
    suspend fun themThietBi(thietBi: ThietBi)

    @Update
    suspend fun suaThietBi(thietBi: ThietBi)

    @Delete
    suspend fun xoaThietBi(thietBi: ThietBi)

    @Query("select * from tb_thiet_bi")
    fun layTatCaThietBi(): LiveData<List<ThietBi>>

    @Query("select * from tb_thiet_bi where seri =:seri")
    fun layThietBiTheoSeri(seri: Int): LiveData<ThietBi>

    @Query("select * from tb_thiet_bi where  tinhTrangSuDung < 10")
    fun layThietBiThanhLy(): LiveData<List<ThietBi>>

    //Phong Ban
    @Insert
    suspend fun themPhongBan(phongBan: PhongBan)

    @Update
    suspend fun suaPhongBan(phongBan: PhongBan)

    @Delete
    suspend fun xoaPhongBan(phongBan: PhongBan)

    @Query("select * from tb_phong")
    fun layTatCaPhongBan(): LiveData<List<PhongBan>>


    //Chinh Sua
    @Insert
    suspend fun themChinhSua(chinhSua: ChiTietChinhSua)

    @Update
    suspend fun suaChinhSua(chinhSua: ChiTietChinhSua)

    @Delete
    suspend fun xoaChinhSua(chinhSua: ChiTietChinhSua)

    @Query("select * from tb_chinh_sua")
    fun layTatCaChinhSua(): LiveData<List<ChiTietChinhSua>>

    @Query("select * from tb_chinh_sua where seriThietBi =:seri")
    fun layChinhSuaTheoThietBi(seri: Int): LiveData<List<ChiTietChinhSua>>


    //Loai Thiet Bi
    @Insert
    suspend fun themLoaiThietBi(loaiThietBi: LoaiThietBi)

    @Update
    suspend fun suaLoaiThietBi(loaiThietBi: LoaiThietBi)

    @Delete
    suspend fun xoaLoaiThietBi(loaiThietBi: LoaiThietBi)

    @Query("select * from tb_loai_tb")
    fun layTatCaLoaiThietBi(): LiveData<List<LoaiThietBi>>

    //Kho
    @Insert
    suspend fun themKho(kho: Kho)

    @Update
    suspend fun suaKho(kho: Kho)

    @Delete
    suspend fun xoaKho(kho: Kho)

    @Query("select * from tb_kho")
    fun layTatCaKho(): LiveData<List<Kho>>

    // Hang San Xuat
    @Insert
    suspend fun themHangSX(hangsx: HangSanXuat)

    @Update
    suspend fun suaHangSX(hangsx: HangSanXuat)

    @Delete
    suspend fun xoaHangSX(hangsx: HangSanXuat)

    @Query("select * from tb_hang_sx")
    fun layTatCaHangSanXuat() : LiveData<List<HangSanXuat>>



}