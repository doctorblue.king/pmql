package com.doctorblue.qlvt.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.doctorblue.qlvt.data.*
import com.doctorblue.qlvt.database.dao.VatTuDao

@Database(
    entities = [ThietBi::class,
        PhongBan::class,
        ChiTietChinhSua::class,
        Kho::class,
        LoaiThietBi::class,
        HangSanXuat::class],
    version = 1,
    exportSchema = true
)
abstract class VatTuDatabase : RoomDatabase() {
    abstract fun getVatTuDao(): VatTuDao

    companion object {
        @Volatile
        private var instance: VatTuDatabase? = null

        fun getInstance(context: Context): VatTuDatabase {
            if (instance == null) {
                instance =
                    Room.databaseBuilder(
                        context,
                        VatTuDatabase::class.java,
                        "VatTuDB"
                    ).build()
            }
            return instance!!
        }

    }

}